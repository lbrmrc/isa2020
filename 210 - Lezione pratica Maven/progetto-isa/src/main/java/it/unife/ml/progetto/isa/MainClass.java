/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.progetto.isa;

/**
 *
 * Questa è la main class.
 * 
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainClass {
    
    /**
     * Questo metodo è utile quando non si vuole fare nulla.
     * 
     */
    public void metodoInutile() {}
    
}
