import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("274969d6-4831-4bcb-9a12-a8d32133c6dc")
public interface Umano extends Personaggio {
    @objid ("ebb0b1d9-8b03-4ba7-a2e0-d76b0d81afee")
    void combatti();

}
