import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("a62af701-5ef5-48cb-bf29-46fa0b5dc54d")
public class Vampiro implements Mostro {
    @objid ("47017aad-dbe2-46d1-bf86-2f095a267672")
    protected String forza;

    @objid ("c22ac38e-1f95-45a8-b351-c9d141d0c2c3")
    public Vampiro() {
    }

    @objid ("efecd387-ba28-4669-9857-f4f59b540c1e")
    public void azzanna() {
    }

    @objid ("7fa0635c-b265-4f53-a30f-684ae2978566")
    public String getForza() {
    }

}
