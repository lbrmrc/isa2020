import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("e3dd1799-386f-43f5-860b-9c26a7b1e842")
public class Licantropo implements Umano, Mostro {
    @objid ("f2d240dd-010f-4a76-9ca0-9d39043b79c9")
    private boolean isUomo;

    @objid ("5abad632-aa16-49b8-aece-7e7e824b1a9f")
    public int forzaMostro;

    @objid ("bf2e2bb7-8b34-4be4-bc11-3411f834ae6c")
    protected int forzaUomo;

    @objid ("704f3288-8756-40d1-9e2d-6b456eb06a3b")
    public Licantropo() {
    }

    @objid ("0054f6c6-3e66-43be-ba60-219e149b0dc7")
    public void setIsUomo(boolean isUomo) {
    }

    @objid ("cebbaaa7-2d5b-46df-84b5-65a50a0302b1")
    public void combatti() {
    }

    @objid ("e16bdd91-ac14-4492-ad8e-75853f69564f")
    public void azzanna() {
    }

    @objid ("579d4aee-9ec3-4e23-a1bc-801b6d463be5")
    public String getForza() {
    }

}
