import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("06106421-9249-42bb-9530-83955e36099a")
public interface Personaggio {
    @objid ("6db32ca9-feef-4cb6-9cd5-442e761f8a67")
    String getForza();

}
