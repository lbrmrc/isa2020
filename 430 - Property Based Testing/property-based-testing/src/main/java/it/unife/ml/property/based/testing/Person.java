/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.property.based.testing;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Person {

    public enum TimeOfLife {
        CHILD, // 0-12 anni
        TEEN, // 13-19 anni
        ADULT, // 20-74 anni
        OLD // 75 anni in poi
    };

    private TimeOfLife timeOfLife;

    private String name;

    private int age;

    public Person(String name, int age) {
        this.name = name;

        if (age < 0) {
            throw new IllegalArgumentException("Age must be >= 0!");
        } else if (age < 13) {
            timeOfLife = TimeOfLife.CHILD;
        } else if (age < 20) {
            timeOfLife = TimeOfLife.TEEN;
        } else if (age < 75) {
            timeOfLife = TimeOfLife.ADULT;
        } else {
            timeOfLife = TimeOfLife.OLD;
        }

    }

    /**
     * @return the timeOfLife
     */
    public TimeOfLife getTimeOfLife() {
        return timeOfLife;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

}
