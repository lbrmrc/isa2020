/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.property.based.testing;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
@RunWith(JUnitQuickcheck.class)
public class PersonTest {

    public PersonTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getTimeOfLife method, of class Person.
     */
    @Ignore // @Ignore in JUnit 4 corrisponde a @Disabled in JUnit 5
    @Test
    public void testGetTimeOfLife() {
        System.out.println("getTimeOfLife");
        Person instance = new Person("Giulio", 5);
        Person.TimeOfLife expResult = Person.TimeOfLife.CHILD;
        Person.TimeOfLife result = instance.getTimeOfLife();
        assertEquals(expResult, result);
    }

    @Property(trials = 100)
    public void propertyTestChild(@From(StringGenerator.class) String name, @InRange(minInt = 0, maxInt = 12) int age) {
//        System.out.println("Fase della vita bambino");
        System.out.println("Nome: " + name + " età: " + age);
        Person instance = new Person(name, age);
        Person.TimeOfLife expResult = Person.TimeOfLife.CHILD;
        Person.TimeOfLife result = instance.getTimeOfLife();
        assertEquals(expResult, result);
    }

    @Property
    public void propertyTestTeen(@From(StringGenerator.class) String name, @InRange(minInt = 13, maxInt = 19) int age) {
//        System.out.println("Fase della vita teenager");
        Person instance = new Person(name, age);
        Person.TimeOfLife expResult = Person.TimeOfLife.TEEN;
        Person.TimeOfLife result = instance.getTimeOfLife();
        assertEquals(expResult, result);
    }

    @Property
    public void propertyTestAdult(@From(StringGenerator.class) String name, @InRange(minInt = 20, maxInt = 74) int age) {
//        System.out.println("Fase della vita adulto");
        Person instance = new Person(name, age);
        Person.TimeOfLife expResult = Person.TimeOfLife.ADULT;
        Person.TimeOfLife result = instance.getTimeOfLife();
        assertEquals(expResult, result);
    }

    @Property
    public void propertyTestOld(@From(StringGenerator.class) String name, @InRange(minInt = 75) int age) {
//        System.out.println("Fase della vita anziano");
        Person instance = new Person(name, age);
        Person.TimeOfLife expResult = Person.TimeOfLife.OLD;
        Person.TimeOfLife result = instance.getTimeOfLife();
        assertEquals(expResult, result);
    }
    
    @Property
    public void propertyTestIllegalAge(@From(StringGenerator.class) String name, @InRange(maxInt=-1) int age) {
        IllegalArgumentException illegalArgumentException = null;
        
        try {
            Person instance = new Person(name, age);
        } catch (IllegalArgumentException ex) {
            illegalArgumentException = ex;
        }
        
        assertNotNull(illegalArgumentException);
        
    }

}
