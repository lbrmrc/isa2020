/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.property.based.testing;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class StringGenerator extends Generator<String> {

        
    public StringGenerator() {
        super(String.class);
    }

    @Override
    public String generate(SourceOfRandomness sor, GenerationStatus gs) {
        StringBuilder sb = new StringBuilder();
        
        sb.append(sor.nextChar('A', 'Z'));
        
        for (int i = 0; i < 14; i++) {
            sb.append(sor.nextChar('a', 'z'));
        }
        
        return sb.toString();
    }
    
}
