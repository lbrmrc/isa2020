/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.junit_mockito;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ClassUsesCounterTest {
    
    public ClassUsesCounterTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of multiplyCounterValue method, of class ClassUsesCounter.
     */
    @Test
    public void testMultiplyCounterValue() {
        System.out.println("multiplyCounterValue");
        int multiplier = 2;
        CounterInterface mock = mock(CounterInterface.class);
                
        ClassUsesCounter instance = new ClassUsesCounter(mock);
        
        when(mock.getValue()).thenReturn(6).thenReturn(7);
        int expResult = 12;
        int result = instance.multiplyCounterValue(multiplier);
        assertEquals(expResult, result);
        
        expResult = 14;
        result = instance.multiplyCounterValue(multiplier);
        assertEquals(expResult, result);
    }
    
}
