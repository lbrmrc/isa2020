/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.junit_mockito;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Timeout;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class CounterTest {

    public CounterTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of increase method, of class Counter.
     */
    @Test
    public void testIncrease() {
        System.out.println("increase");
        Counter instance = new Counter(10);
        instance.increase();
        int expectedValue = 11;
        assertEquals(expectedValue, instance.getValue());
    }

    /**
     * Test of decrease method, of class Counter.
     */
    @Test
    public void testDecrease() {
        System.out.println("decrease");
        Counter instance = new Counter(0);
        instance.decrease();
        int expectedValue = 0;
        assertEquals(expectedValue, instance.getValue());
    }

    /**
     * Test of getValue method, of class Counter.
     */
    @Disabled
    @Test
    public void testTimeout() {
        assertTimeoutPreemptively(Duration.ofSeconds(1), () -> {
            TimeUnit.SECONDS.sleep(5);
        });
        
        // altro codice fuori dai limiti del timeout
    }

    @Disabled
    @Test
    @Timeout(value = 1, unit = TimeUnit.SECONDS)
    public void testTimeout2() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
    }

}
