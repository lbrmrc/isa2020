/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.junit_mockito;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface CounterInterface {
    
    /**
     * Incrementa il contatore.
     */
    public void increase();
    
    
    /**
     * Decrementa il contatore
     */
    public void decrease();
    
    /**
     * restituisce il contatore
     * @return 
     */
    public int getValue();
    
}
