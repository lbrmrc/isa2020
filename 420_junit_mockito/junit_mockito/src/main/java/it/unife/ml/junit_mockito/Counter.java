/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.junit_mockito;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Counter implements CounterInterface {

    protected int value;

    Counter(int value) {
        this.value = value;
    }

    @Override
    public void increase() {
        value++;
    }

    @Override
    public void decrease() {
        if (value > 0) {
            value--;
        }
    }

    @Override
    public int getValue() {
        return value;
    }

}
