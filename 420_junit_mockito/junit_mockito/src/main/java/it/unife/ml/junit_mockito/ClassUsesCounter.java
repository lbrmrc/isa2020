/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.junit_mockito;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ClassUsesCounter {
    
    private CounterInterface counter;
    
    public ClassUsesCounter(CounterInterface counter) {
        this.counter = counter;
    }
    
    public int multiplyCounterValue(int multiplier) {
        return multiplier * counter.getValue();
    }
    
}
