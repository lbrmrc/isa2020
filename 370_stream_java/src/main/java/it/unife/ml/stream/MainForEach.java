/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainForEach {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Stream<String> wordsStream = Stream.of("Mi", "chiamo", "Giuseppe", "Cota");
        List<String> words = new ArrayList<>();
        words.add("Mi");
        words.add("chiamo");
        words.add("Giovanni");
        words.add("Giacomo");
        words.add("Giuseppe");
        Stream<String> wordsStream2 = words.stream();
        Stream<String> wordsParallelStream = words.parallelStream();
        
        System.out.println("Stampo primo stream");
        wordsStream.forEach(s -> System.out.print(s));
        System.out.println();
        
        System.out.println("Stampo secondo stream");
        wordsStream2.forEach(System.out::print);
        System.out.println();
        
        System.out.println("Stampo terzo stream parallelo");
        wordsParallelStream.forEach(System.out::print);
        System.out.println();
        
        
        
    }
    
}
