/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainMap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("Mi");
        words.add("chiamo");
        words.add("Giovanni");
        words.add("Giacomo");
        words.add("Giuseppe");

        Stream<String> lowerCaseStream = words.stream().map(String::toLowerCase);
        lowerCaseStream.forEach(System.out::println);
        System.out.println("\n");
       
        Stream<String> firstLettersStream = words.stream().map(s -> s.substring(0, 1));
        firstLettersStream.forEach(System.out::println);
        
    }
    
}
