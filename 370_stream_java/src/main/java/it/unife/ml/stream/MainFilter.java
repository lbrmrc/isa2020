/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainFilter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("Mi");
        words.add("chiamo");
        words.add("Giovanni");
        words.add("Giacomo");
        words.add("Giuseppe");
        
        Stream<String> longWordsStream = words.stream().filter((s) -> {return s.length() > 5;});
        longWordsStream.forEach(System.out::println);
        System.out.println("\n");
        
        Stream<String> longGWordsStream = words.stream()
                .filter(s -> s.length() > 5)
                .filter(s -> s.startsWith("G"));
        longGWordsStream.forEach(System.out::println);
    }

}
