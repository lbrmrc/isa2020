/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainReduce {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("Mi");
        words.add("chiamo");
        words.add("Giovanni");
        words.add("Giacomo");
        words.add("Giuseppe");
        words.add("Giulio");
        words.add("Gioacchino");
        
        long count = words.stream().filter(s -> s.length() > 6).count();
        System.out.println("Ci sono " + count + " parole più lunghe di 6 lettere\n");
        
        Optional<String> startsWithGFirst = words.stream()
                .filter(s -> s.startsWith("G")).findFirst();
        System.out.println("Prima parola che inizia con G: " + startsWithGFirst.get() + "\n");
        
        Optional<String> startsWithGAny = words.stream()
                .filter(s -> s.startsWith("G")).findAny();
        System.out.println("Qualunque parola che inizia con G: "+ startsWithGAny.get() + "\n");
        
        Optional<String> startsWithQFirst = words.stream()
                .filter(s -> s.startsWith("Q")).findFirst();
        System.out.println("Prima parola che inizia con Q: " + startsWithQFirst.orElse("") + "\n");
        
        System.out.println("Prima parola che inizia con Q: " + startsWithQFirst.orElseGet(() -> System.getProperty("user.name")) + "\n");
        
        
        Integer[] arrInt = new Integer[]{1, 2, 3, 4, 5};
        
        Optional<Integer> sum = Stream.of(arrInt).reduce((x, y) -> x + y);
        System.out.println(sum.orElse(0));

    }

}
