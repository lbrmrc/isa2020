/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.altri_design_pattern;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LettoreMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        String fileName = "testo.txt";
        Copy copy = new Copy(new LineReader(fileName));
        
        // l'utente sceglie un converter che implementa ConversionStrategy
        // ... 
        
        ConversionStrategy cs = new UpperCaseConverter();
        copy.toOutput(new StraightLineWriter(System.out, cs));
    }
    
}
