/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.altri_design_pattern;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class LineWriter {
    
    private PrintStream out;
    private ConversionStrategy fConverter;
    
    public LineWriter(PrintStream out, ConversionStrategy fConverter) {
        this.out = out;
        this.fConverter = fConverter;
    }
    
    public void printAllLines(List<String> storage) {
        Iterator<String> i = getIterator(storage);
        
        while (i.hasNext()) {
            String line = i.next().toString();
            out.println(fConverter.convert(line));
        }
    }
    
    protected abstract Iterator<String> getIterator(List<String> storage);
    
}
