/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.altri_design_pattern;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class StraightLineWriter extends LineWriter {
    
    public StraightLineWriter(PrintStream out, ConversionStrategy fConverter){
        super(out, fConverter);
    }

    @Override
    protected Iterator<String> getIterator(List<String> storage) {
        return storage.iterator();
    }
    
    
}
