package it.unife.ml.decorator_pattern;

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package it.unife.ml.altri_design_pattern;
//
//import java.io.BufferedReader;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
///**
// *
// * @author Giuseppe Cota <giuseppe.cota@unife.it>
// */
//public class Lettore {
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        String fileName = "testo.txt";
//        List<String> storage = new ArrayList<>();
//        try {
//            BufferedReader reader = new BufferedReader(new FileReader(fileName));
//            String line = reader.readLine();
//            while (line != null) {
//                storage.add(line);
//                line = reader.readLine();
//            }
//            
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        
//        Iterator i = storage.iterator();
//        while (i.hasNext()) {
//            System.out.println(i.next());
//        }
//        
//    }
//    
//}
