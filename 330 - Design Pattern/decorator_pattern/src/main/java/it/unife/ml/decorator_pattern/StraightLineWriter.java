/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.decorator_pattern;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class StraightLineWriter implements LineWriter {

    public StraightLineWriter(PrintStream out, ConversionStrategy fConverter) {
        this.out = out;
        this.fConverter = fConverter;
    }

    private PrintStream out;
    private ConversionStrategy fConverter;

    @Override
    public void printAllLines(List<String> storage) {
        Iterator<String> i = getIterator(storage);

        while (i.hasNext()) {
            String line = i.next();
            out.println(fConverter.convert(line));
        }
    }

    @Override
    public Iterator<String> getIterator(List<String> storage) {
        return storage.iterator();
    }

}
