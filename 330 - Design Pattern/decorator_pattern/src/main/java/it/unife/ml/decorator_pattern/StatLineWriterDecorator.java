/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.decorator_pattern;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class StatLineWriterDecorator extends LineWriterDecorator {

    public StatLineWriterDecorator(LineWriter lw) {
        super(lw);
    }

    @Override
    public void printAllLines(List<String> storage) {
        super.printAllLines(storage);
        makeStatistics(storage);
    }

    private void makeStatistics(List<String> storage) {
        int tot = 0;
        Iterator<String> it = storage.iterator();
        
        while(it.hasNext()) {
            tot += it.next().length();
        }
        
        System.out.println("Numero di caratteri totale: " + tot);
    }

}
