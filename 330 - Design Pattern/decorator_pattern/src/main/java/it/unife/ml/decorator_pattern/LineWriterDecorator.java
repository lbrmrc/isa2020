/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.decorator_pattern;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LineWriterDecorator implements LineWriter {
    
    protected LineWriter lw;
    
    public LineWriterDecorator(LineWriter lw) {
        this.lw = lw;
    }
    
    @Override
    public void printAllLines(List<String> storage) {
        lw.printAllLines(storage);
    }
    
    @Override
    public Iterator<String> getIterator(List<String> storage) {
        return lw.getIterator(storage);
    }
    
}
