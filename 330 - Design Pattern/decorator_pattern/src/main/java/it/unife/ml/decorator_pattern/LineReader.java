/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.decorator_pattern;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Classe che legge le righe di un file e le mette in una lista.
 * 
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LineReader {
    
    private BufferedReader reader; 
    
    public LineReader(String fileName) {
        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    public void readAllLines(List<String> storage) {
        try {
            String line = reader.readLine();
            while (line != null) {
                storage.add(line);
                line = reader.readLine();
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
