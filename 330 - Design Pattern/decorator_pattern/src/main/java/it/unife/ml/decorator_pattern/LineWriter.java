/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.decorator_pattern;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface LineWriter {
    
    public void printAllLines(List<String> storage);
    
    public Iterator<String> getIterator(List<String> storage);
    
}
