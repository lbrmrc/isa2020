/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.decorator_pattern;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Copy {
    
    protected List<String> storage;
    
    public Copy(LineReader in) {
        storage = new ArrayList<>();
        in.readAllLines(storage);
    }
    
    public void toOutput(LineWriter out) {
        out.printAllLines(storage);
    }
    
}
