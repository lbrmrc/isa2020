/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.design_pattern_isa;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MyListIterator implements MyIterator {

    private int indice = -1;
    private int[] lista;
    
    public MyListIterator(MyListArray la) {
        this.lista = la.getLista();
    }

    @Override
    public void rewind() {
        indice = -1;
    }

    @Override
    public int nextElement() {
        indice++;
        return lista[indice];
    }

    @Override
    public boolean hasNextElement() {
        return indice < lista.length - 1;
    }

}
