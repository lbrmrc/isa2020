/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.design_pattern_isa;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface MyIterator {
    
    public void rewind();
    
    public int nextElement();
    
    public boolean hasNextElement();
    
}
