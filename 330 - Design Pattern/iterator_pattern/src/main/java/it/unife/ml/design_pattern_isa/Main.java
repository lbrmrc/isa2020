/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.design_pattern_isa;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyListArray la = new MyListArray(10);
        
        MyIterator li = la.getIter();
        
        while (li.hasNextElement()) {
            System.out.println("El. " + li.nextElement());
        }
    }
    
}
