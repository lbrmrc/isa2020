/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.design_pattern_isa;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MyListArray {

    private int[] lista;


    public MyListArray(int n) {
        lista = new int[n];

        for (int i = 0; i < n; i++) {
            lista[i] = i * 10;
        }
    }
    
    public int[] getLista() {
        return this.lista;
    }

    public MyIterator getIter() {
        return new MyListIterator(this);
    }
    

}
