/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.strategy_lambda;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class UpperCaseConverter implements ConversionStrategy {

    @Override
    public String convert(String string) {
        return string.toUpperCase();
    }
    
}
