/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.strategy_lambda;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainStrategyLambda {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s = "Mi chiamo Giuseppe";
        System.out.println(s);
        ConversionStrategy cs1 = (String stringa) -> {return stringa;};
        System.out.println(cs1.convert(s));
        ConversionStrategy cs2 = stringa -> stringa.toUpperCase();
        System.out.println(cs2.convert(s));
    }
    
}
